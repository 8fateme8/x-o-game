import EnumsClass.EOX;
import Functions.Game;
import Functions.Players;
import Functions.TicTacToe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static Players players1 = new Players(EOX.X);
    static Players players2 = new Players(EOX.O);
    static Game game = new Game();
    static TicTacToe ticTacToe = new TicTacToe(game, players1, players2);

    public static void main(String[] args) {
        game.setTicTacToe(ticTacToe);
        System.out.println(ticTacToe);
        while (!game.isEndGame()) {
            while (!game.isEndGame()) {
                try {
                    if (message('X', players1))
                        break;
                } catch (InputMismatchException e) {
                    System.err.println("enter right format");
                    scanner.next();
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.err.println("enter number between 0 and 2");
                }
            }
            while (!game.isEndGame()) {
                try {
                    if (message('O', players2))
                        break;
                } catch (InputMismatchException e) {
                    System.err.println("enter right format");
                    scanner.next();
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.err.println("enter number between 0 and 2");
                }
            }
        }
    }

    private static boolean message(char ch, Players players) {
        System.out.println(ch + " turn :");
        if (ticTacToe.setMove(scanner.nextInt(), scanner.nextInt(), players)) {
            System.out.println(ticTacToe);
            return true;
        } else {
            System.out.println(ticTacToe);
            return false;
        }
    }
}
