package Functions;

import EnumsClass.EOX;

public class Players {
    private EOX move;

    public Players(EOX move) {
        this.move = move;
    }

    public EOX getMove() {
        return move;
    }

    @Override
    public String toString() {
        return "Player "+ move + " WIns";
    }
}
