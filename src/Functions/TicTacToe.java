package Functions;

import EnumsClass.EOX;
import EnumsClass.Situation;

public class TicTacToe {
    private EOX[][] gamePage;
    private Players players1, players2;
    private EOX E;
    private Game game;

    public TicTacToe(Game game, Players players1, Players players2) {
        this.E = EOX.E;
        this.players1 = players1;
        this.players2 = players2;
        this.gamePage = new EOX[][]{{E, E, E}, {E, E, E}, {E, E, E}};
        this.game = game;
    }

    public boolean setMove(int row, int column, Players players) throws ArrayIndexOutOfBoundsException {
        boolean isAdd = this.game.addMove(players, row, column);
        if (isAdd) {
            if (this.game.isEndGame()){
                String wins = game.getSituation() == Situation.DRAW ? "Draw ":
                        (game.getSituation() == Situation.X_WIN ? players1.toString() : players2.toString());
                System.out.println(wins);
            }
            return true;
        } else return false;
    }

    public EOX[][] getGamePage() {
        return gamePage;
    }


    @Override
    public String toString() {
        String output = "";
        for (int i = 0; i < gamePage.length; i++) {
            for (int j = 0; j < gamePage.length; j++)
                output += gamePage[i][j] + " ";
            output += "\n";
        }
        return output;
    }
}
