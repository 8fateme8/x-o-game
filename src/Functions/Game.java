package Functions;

import EnumsClass.EOX;
import EnumsClass.Situation;

public class Game {
    private boolean endGame = false;
    private Situation situation;
    private int countE = 9;
    private TicTacToe ticTacToe;

    public void setTicTacToe(TicTacToe ticTacToe) {
        this.ticTacToe = ticTacToe;
    }

    public boolean isEndGame() {
        return endGame;
    }

    public Situation getSituation() {
        return situation;
    }

    public boolean addMove(Players players, int row, int column) throws ArrayIndexOutOfBoundsException {
        EOX eox = this.ticTacToe.getGamePage()[row][column];
        if (eox == EOX.E) {
            ticTacToe.getGamePage()[row][column] = players.getMove();
            countE--;
            isFinished();
            return true;
        } else {
            return false;
        }
    }

    private void isFinished() {
        switch (checkSituation()) {
            case X_WIN:
                endGame = true;
                situation = Situation.X_WIN;
                break;

            case O_WIN:
                endGame = true;
                situation = Situation.O_WIN;
                break;

            default:
                if (countE == 0)
                    endGame = true;
                situation = Situation.DRAW;
                break;
        }
    }

    private Situation checkSituation() {
        EOX[][] gamePage = ticTacToe.getGamePage();
        if (checkRow(gamePage) == null) {
            Situation situation = checkColumn(gamePage);
            return (situation == null) ? Situation.DRAW : ((situation == Situation.X_WIN) ? Situation.X_WIN : Situation.O_WIN);
        } else if (checkColumn(gamePage) == null) {
            Situation situation = checkRow(gamePage);
            return (situation == null) ? Situation.DRAW : ((situation == Situation.X_WIN) ? Situation.X_WIN : Situation.O_WIN);
        }
        return null;
    }

    private Situation checkRow(EOX[][] gamePage) {
        int xMOve = 0, oMove = 0;
        for (int i = 0; i < gamePage.length; i++) {
            for (int j = 0; j < gamePage.length; j++) {
                if (EOX.X == gamePage[i][j]) {
                    if (oMove == 0)
                        xMOve++;
                    else {
                        oMove = 0;
                        break;
                    }
                } else if (EOX.O == gamePage[i][j]) {
                    if (xMOve == 0)
                        oMove++;
                    else {
                        xMOve = 0;
                        break;
                    }
                } else {
                    oMove = 0;
                    xMOve = 0;
                    break;
                }
            }
            if (xMOve == 3)
                return Situation.X_WIN;
            else if (oMove == 3)
                return Situation.O_WIN;
        }
        return null;
    }

    private Situation checkColumn(EOX[][] gamePage) {
        int xMOve = 0, oMove = 0;
        for (int i = 0; i < gamePage.length; i++) {
            for (int j = 0; j < gamePage.length; j++) {
                if (EOX.X == gamePage[j][i]) {
                    if (oMove == 0)
                        xMOve++;
                    else {
                        oMove = 0;
                        break;
                    }
                } else if (EOX.O == gamePage[j][i]) {
                    if (xMOve == 0)
                        oMove++;
                    else {
                        xMOve = 0;
                        break;
                    }
                } else {
                    oMove = 0;
                    xMOve = 0;
                    break;
                }
            }
            if (xMOve == 3)
                return Situation.X_WIN;
            else if (oMove == 3)
                return Situation.O_WIN;
        }
        return null;
    }


}
